layui.use(['layer', 'jquery', 'form'], function () {
    var form = layui.form();
    var layer = layui.layer;
    var $ = layui.jquery;

    form.on('submit(joinSubmit)', function (data) {
        $.post('/party/join', data.field, function (data) {
            if (!data.status) {
                layer.open({title: '警告', content: data.msg()});
            } else {
                layer.open({title: '警告', content: "报名成功"});
            }
        });
        return false;
    });

    form.on('submit(allowSubmit)', function (data) {
        $.post('/party/allow', data.field, function (data) {
            if (!data.status) {
                layer.open({title: '警告', content: data.msg()});
            } else {
                layer.open({title: '警告', content: "已允许"});
                location.reload();
            }
        });
        return false;
    });

});