package com.forrest.party;

import com.forrest.common.model.Party;
import com.jfinal.core.Controller;

import java.util.List;

public class PartyController extends Controller {

    private static final PartyService srv = PartyService.me;

    public void index() {
        List<Party> partyList = srv.display();
        for (Party aPartyList : partyList) {
            aPartyList.setParticipant(srv.participant(aPartyList.getId()));
        }
        setAttr("partylist", partyList);
        render("index.html");
    }

    public void join() {
        int partyid = getParaToInt("party");
        String participant = getPara("participant");
        renderJson(srv.joinParty(partyid, participant, 0));
    }

    public void manage() {
        List<Party> partyList = srv.manage();
        if (!partyList.isEmpty()) {
            setAttr("partylist", partyList);
        }
        render("manage.html");
    }

    public void allow() {
        int id = getParaToInt("party");
        renderJson(srv.allow(id));
    }

}
