package com.forrest.common.interceptor;

import com.forrest.common.model.User;
import com.forrest.login.LoginService;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.plugin.redis.Redis;

public class GenderInterceptor implements Interceptor {

    public void intercept(Invocation inv) {
        Controller c = inv.getController();
        String token = c.getCookie(LoginService.USER);
        User user = token == null ? null : Redis.use().get(token);
        if (null != user && !"不详".equals(user.getGender())) {
            if ("男".equals(user.getGender())) {
                c.setAttr("male", "checked");
            } else if ("女".equals(user.getGender())) {
                c.setAttr("female", "checked");
            }
        }
        inv.invoke();
    }

}
