package com.forrest.common;

import com.forrest.common.interceptor.UserInterceptor;
import com.forrest.common.model._MappingKit;
import com.jfinal.config.*;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.redis.RedisPlugin;
import com.jfinal.template.Engine;

public class BaseConfig extends JFinalConfig {

    private static final Prop p = PropKit.use("config.properties");

    @Override
    public void configConstant(Constants me) {
        me.setDevMode(p.getBoolean("devMode", false));
        me.setBaseUploadPath(p.get("baseUploadPath"));
    }

    @Override
    public void configHandler(Handlers me) {

    }

    public void configInterceptor(Interceptors me) {
        me.add(new UserInterceptor());
    }

    @Override
    public void configRoute(Routes me) {
        me.add(new FrontRoutes());
    }

    @Override
    public void configEngine(Engine me) {
        me.addSharedFunction("/_view/common/layout.html");
    }

    public DruidPlugin getDruidPlugin() {
        return new DruidPlugin(p.get("jdbcUrl"),
                p.get("user"), p.get("password"));
    }

    @Override
    public void configPlugin(Plugins me) {
        DruidPlugin dp = getDruidPlugin();
        me.add(dp);
        ActiveRecordPlugin arp = new ActiveRecordPlugin(dp);
        me.add(arp);
        _MappingKit.mapping(arp);
        arp.setBaseSqlTemplatePath(PathKit.getRootClassPath() + "/sql");
        arp.addSqlTemplate("all.sql");
        RedisPlugin pmRedis = new RedisPlugin("pm", "localhost", 6379, 900);
        me.add(pmRedis);
    }

    @Override
    public void afterJFinalStart() {

    }

}
