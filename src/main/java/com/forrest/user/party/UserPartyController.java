package com.forrest.user.party;

import com.forrest.common.interceptor.PartyInterceptor;
import com.forrest.common.interceptor.UserInterceptor;
import com.forrest.common.model.Party;
import com.forrest.common.model.User;
import com.forrest.login.LoginService;
import com.forrest.party.PartyService;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;

import java.util.List;

public class UserPartyController extends Controller {

    private static final PartyService srv = PartyService.me;

    @Before({UserInterceptor.class, PartyInterceptor.class})
    public void index() {
        User user = LoginService.me.validateToken(getCookie(LoginService.USER));
        List<Party> myOwn = srv.myOwn(user.getUsername());
        List<Party> waiting = srv.waiting(user.getUsername());
        List<Party> myJoin = srv.myJoin(user.getUsername());
        if (!myOwn.isEmpty()) {
            setAttr("myOwn", myOwn);
        }
        if (!myJoin.isEmpty()) {
            setAttr("myJoin", myJoin);
        }
        if (!waiting.isEmpty()) {
            setAttr("waiting", waiting);
        }

        render("index.html");
    }

}
