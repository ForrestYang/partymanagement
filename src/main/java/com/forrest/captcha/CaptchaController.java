package com.forrest.captcha;

import com.jfinal.core.Controller;

/**
 * This is the captcha controller.
 *
 * @author Forrest
 * @version 1.0.0
 * @since 1.0.0
 */
public class CaptchaController extends Controller {

    /**
     * 图片验证码
     */
    public void image() {
        renderCaptcha();
    }

}
