package com.forrest.community;

import com.forrest.common.model.User;
import com.jfinal.aop.Duang;

import java.util.List;

public class CommunityService {

    public static final CommunityService me = Duang.duang(CommunityService.class);
    private static final User userDao = new User().dao();

    public List<User> display() {
        return userDao.find(userDao.getSqlPara("user.findAll"));
    }

    public List<User> search(String search) {
        return userDao.find(userDao.getSqlPara("user.search", search));
    }

}
