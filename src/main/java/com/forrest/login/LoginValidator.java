package com.forrest.login;

import com.forrest.common.BaseValidator;
import com.jfinal.core.Controller;

public class LoginValidator extends BaseValidator {

    @Override
    protected void validate(Controller c) {
        validateCaptcha("captcha", "msg", "验证码错误");
        validateRequired("username", "msg", "请输入用户名");
        validateRequired("pwd", "msg", "请输入密码");
    }

}
