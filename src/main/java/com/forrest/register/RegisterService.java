package com.forrest.register;

import com.forrest.common.model.User;
import com.jfinal.aop.Duang;
import com.jfinal.kit.Ret;

public class RegisterService {

    public static final RegisterService me = Duang.duang(RegisterService.class);
    private static final User userDao = new User().dao();

    public Ret check(String username) {
        User user = userDao.findFirst(userDao.getSqlPara("user.findByUsername", username));
        return Ret.by("status", null != user);
    }

    public Ret register(String username, String pwd) {
        User isExists = userDao.findFirst(userDao.getSqlPara("user.findByUsername", username));
        if (null != isExists) {
            return Ret.by("status", false).set("msg", "用户已存在");
        }
        User user = new User();
        if (user.setUsername(username).setPwd(pwd).save()) {
            return Ret.by("status", true);
        } else {
            return Ret.by("status", false).set("msg", "未知错误");
        }
    }

}
