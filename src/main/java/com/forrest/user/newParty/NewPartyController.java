package com.forrest.user.newParty;

import com.forrest.party.PartyService;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.upload.UploadFile;

import java.io.File;
import java.util.Date;

public class NewPartyController extends Controller {

    private static final PartyService srv = PartyService.me;

    public void index() {
        render("index.html");
    }

    public void newParty(String partyname, String partyowner, Date partytime, String partyplace, String avator) {
        renderJson(srv.newParty(partyname, partyowner, partytime, partyplace, avator, 0));
    }

    public void avator() {
        UploadFile uploadFile = getFile("avator", "/partyimg");
        File file = uploadFile.getFile();
        String rename = uploadFile.getFileName();
        File file1 = new File(file.getParent(), "/" + uploadFile.getFileName());
        if (!file.renameTo(file1)) {
            renderJson(Ret.by("status", false).set("msg", "上传错误"));
        } else {
            renderJson(Ret.by("status", true).set("avator", rename));
        }
    }

}
