package com.forrest.user.index;

import com.forrest.common.model.User;
import com.jfinal.aop.Duang;
import com.jfinal.kit.Ret;

public class UserService {

    public static final UserService me = Duang.duang(UserService.class);

    public Ret avator(User user, String avatorName) {
        if (!user.setAvator(avatorName).update()) {
            return Ret.by("status", false).set("msg", "数据库异常");
        } else {
            return Ret.by("status", true);
        }
    }

    public Ret update(User user, String pname, String gender, String phone, String company, String address) {
        if (!user.setPname(pname).setGender(gender).setPhone(phone).setCompany(company).setAddress(address).update()) {
            return Ret.by("status", false).set("msg", "数据库异常");
        } else {
            return Ret.by("status", true);
        }
    }

}
