package com.forrest.common;

import com.forrest.captcha.CaptchaController;
import com.forrest.community.CommunityController;
import com.forrest.index.IndexController;
import com.forrest.login.LoginController;
import com.forrest.party.PartyController;
import com.forrest.register.RegisterController;
import com.forrest.user.edit.UserEditController;
import com.forrest.user.index.UserController;
import com.forrest.user.newParty.NewPartyController;
import com.forrest.user.party.UserPartyController;
import com.jfinal.config.Routes;

public class FrontRoutes extends Routes {

    @Override
    public void config() {
        setBaseViewPath("_view");
        add("/", IndexController.class, "/index");
        add("/login", LoginController.class);
        add("/captcha", CaptchaController.class);
        add("/register", RegisterController.class);
        add("/user", UserController.class);
        add("/user/edit", UserEditController.class);
        add("/community", CommunityController.class);
        add("/user/party", UserPartyController.class);
        add("/user/newParty", NewPartyController.class);
        add("/party", PartyController.class);
    }

}
