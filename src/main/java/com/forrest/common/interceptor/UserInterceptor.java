package com.forrest.common.interceptor;

import com.forrest.common.model.User;
import com.forrest.login.LoginService;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

public class UserInterceptor implements Interceptor {

    public void intercept(Invocation inv) {
        Controller c = inv.getController();
        User user = LoginService.me.validateToken(c.getCookie(LoginService.USER));
        if (null != user) {
            if (user.getManager()) {
                c.setAttr("manager", user);
            }
            c.setAttr("user", user);

            if (null == user.getPname()) {
                c.setAttr("username", user.getUsername());
            } else {
                c.setAttr("username", user.getPname());
            }
        }
        inv.invoke();
    }

}
