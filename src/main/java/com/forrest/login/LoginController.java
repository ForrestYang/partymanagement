package com.forrest.login;


import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;

public class LoginController extends Controller {

    private LoginService srv = LoginService.me;

    public void index() {
        render("index.html");
    }

    @Before(LoginValidator.class)
    public void login(String username, String pwd) {
        Ret ret = srv.login(username, pwd);
        if (ret.getBoolean("status")) {
            setCookie(LoginService.USER, ret.getStr("token"), 60 * 60);
        }
        renderJson(ret);
    }

    public void logOut() {
        removeCookie(LoginService.USER);
        redirect("/");
    }

}
