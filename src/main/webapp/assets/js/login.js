layui.use(['form', 'layer'], function () {
    var form = layui.form();
    var layer = layui.layer;
    var $ = layui.jquery;

    var captchaImg = $('.captcha');

    var updateCaptcha = function () {
        captchaImg.prop('src', '/captcha/image?v=' + Math.random());
    };

    updateCaptcha();

    captchaImg.click(function () {
        updateCaptcha();
    });


    form.on('submit(loginSubmit)', function (data) {
        $.post('/login/login', data.field, function (data) {
            if (!data.status) {
                layer.open({title: '警告', content: data.msg});
                updateCaptcha();
            } else {
                location.href = '/';
            }
        });
        return false;
    });

});
