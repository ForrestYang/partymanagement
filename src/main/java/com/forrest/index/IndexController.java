package com.forrest.index;

import com.forrest.common.interceptor.UserInterceptor;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;

@Before(UserInterceptor.class)
public class IndexController extends Controller {

    public void index(){
        render("index.html");
    }

}
