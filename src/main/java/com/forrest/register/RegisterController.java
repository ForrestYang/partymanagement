package com.forrest.register;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;

public class RegisterController extends Controller {

    private static final RegisterService srv = RegisterService.me;

    public void index() {
        render("index.html");
    }

    public void checkUser(String username) {
        renderJson(srv.check(username));
    }

    @Before(RegisterValidator.class)
    public void register(String username, String pwd) {
        renderJson(srv.register(username, pwd));
    }

    public void success() {
        render("success.html");
    }

}
