package com.forrest.register;

import com.forrest.common.BaseValidator;
import com.jfinal.core.Controller;

public class RegisterValidator extends BaseValidator {

    @Override
    protected void validate(Controller c) {
        validateCaptcha("captcha", "msg", "验证码错误");
        validateRequired("username", "msg", "请输入用户名");
        validateRequired("pwd", "msg", "请输入密码");
    }

}
