layui.use(['form', 'upload', 'layer', 'laydate'], function () {
    var form = layui.form();
    var layer = layui.layer;
    var $ = layui.jquery;
    var laydate = layui.laydate;

    layer.open({title: '警告', content: '聚会发起后信息不可更改，请谨慎填写！提交之前记得上传头像哦！'});

    laydate.render({elem: '#party-time', min: 0});

    layui.upload({
        url: '/user/newParty/avator'
        , success: function (res) {
            if (!res.status) {
                layer.open({title: '警告', content: res.msg()});
            } else {
                $('.party-avator').attr("src", "/img/partyimg/" + res.avator);
                $('#party-avator').attr("value", res.avator);
                layer.open({title: '警告', content: '上传成功'});
            }
        }
    });


    form.on('submit(newpartySubmit)', function (data) {
        $.post('/user/newParty/newParty', data.field, function (data) {
            if (!data.status) {
                layer.open({title: '警告', content: data.msg()});
            } else {
                location.href = '/user/party';
            }
        });
        return false;
    });

});