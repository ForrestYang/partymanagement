package com.forrest.common;

import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

public abstract class BaseValidator extends Validator {

    public BaseValidator() {
        shortCircuit = true;
    }

    @Override
    protected void handleError(Controller c) {
        c.setAttr("status", false);
        c.renderJson();
    }

}
