package com.forrest.common.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

public class PartyInterceptor implements Interceptor {

    public void intercept(Invocation inv) {
        inv.invoke();
    }

}
