layui.use(['form', 'upload', 'layer'], function () {
    var form = layui.form();
    var layer = layui.layer;
    var $ = layui.jquery;


    layui.upload({
        url: '/user/edit/avator'
        , success: function (res) {
            if (!res.status) {
                layer.open({title: '警告', content: res.msg()});
            } else {
                location.reload();
            }
        }
    });

    form.on('submit(editSubmit)', function (data) {
        $.post('/user/edit/update', data.field, function (data) {
            console.log("2");
            if (!data.status) {
                layer.open({title: '警告', content: data.msg()});
            } else {
                location.href = '/user';
            }
        });
        return false;
    });

});

