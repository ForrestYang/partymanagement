package com.forrest.community;

import com.forrest.common.model.User;
import com.jfinal.core.Controller;

import java.util.List;

public class CommunityController extends Controller {

    private static final CommunityService srv = CommunityService.me;

    public void index() {
        setAttr("userlist", srv.display());
        render("index.html");
    }

    public void search(String search) {
        List<User> userList = srv.search(search);
        setAttr("searchResult", userList);
        render("result.html");
    }

}
