package com.forrest.user.index;

import com.forrest.common.interceptor.UserInterceptor;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;


public class UserController extends Controller {

    @Before(UserInterceptor.class)
    public void index() {
        render("index.html");
    }

}
